## Introduction ##

A Pregel system with the **channel interface** for composing optimization techniques (where Pregel stands for a class of graph processing systems that adopts the vertex-centric paradigm, BSP model and the message passing interface).

Three branches (master, vertex-program, with-compiler) are maintained for different purposes.

* **[master]**: An experimental framework for trying various optimizations, such as developing new communication channels, hybrid MPI-OpenMP implementation and partitioning.
 It differs from a traditional Pregel-like system in various ways, like a subgraph-level **compute()** function and a slightly different termination mechanism based on each subgraph voting to halt.
 
* **[vertex-program]**: It is basically the same system, but the programming interface is much more friendly for Pregel programmers.
 The performance is slightly sacrificed though.
 **The example below is from this branch.**
 
* **[with-compiler]**: A system particularly designed to serve as the backend of a SQL-like high-level domain-specific language [sql-core](https://bitbucket.org/zyz915/sql-core) for vertex-centric graph processing.
 Special channels are designed and implemented to simplify the evaluation of graph queries.

### What is the channel interface? ###

The channel interface is an extension of Pregel's message passing interface.
Basically, it is a mechanism that allows users to use multiple message buffers in a single Pregel algorithm, such that messages with different types or communication patterns are managed separately.
More precisely, channels are message buffers with a handler for a particular communication pattern.

Our system initially offers the standard message passing channel, the aggregator channel and several optimized channels that can be highly efficient in some situations.
To obtain an efficient program, for each channel in the program, programmers just select a proper implementation that best suits the channel's communication pattern.

How to design the optimizations for Pregel and how they are composed together are two separate questions.
Our system mainly targets the second question by providing the channel mechanism.
The channels are more like plugins to the system, and whenever necessary, one can implement an optimization (to handle some communication pattern) as a new channel by sub-classing the **Channel** class in our system and override its methods.
When using that optimization, only the channel instances switching to that user-implemented channel are affected.
Users can freely choose any channel implementation for each channel in the algorithm, therefore the composition of optimizations comes for free.

Initially, we provide three optimized channels, and this set of optimizations already covers a lot of commonly used Pregel algorithms:

* scatter-combine channel (PageRank, S-V).
* request-respond channel (S-V, MSF).
* propagation channel (SSSP, WCC, SCC).

You can find the implementation of these channels (`utils/`) and examples (`apps/`) in our repository.

### An example: PageRank ###

To give a picture of how programs are constructed in our channel-based system, we present an ordinary implementation of PageRank using two channels.
One is the message passing channel (with a combiner), and the other is an aggregator channel.
**Another optimized version (using a special scatter-combine channel) is also available in our repository**.
We recommend you to have a try!

```
// the user-specified vertex value
struct PRValue {
   double PageRank;
   vector<int> Edges;
};

using VertexT = Vertex<int, PRValue>;
class PageRankWorker : public Worker<VertexT> {
private:
   // two channels are defined here
   CombinedMessage<VertexT, double> ch1;
   Aggregator<VertexT, double> ch2;
public:
   PageRankWorker():
         ch1(this, make_combiner(c_sum, 0.0)),
         ch2(this, make_combiner(c_sum, 0.0)) {}

   void compute(VertexT &v) override {
      if (step_num() == 1) {
         value().PageRank = 1.0 / total_numv();
      } else {
         double s = ch2.result() / total_numv();
         value().PageRank = 0.15 / total_numv()
               + 0.85 * (ch1.get_message() + s);
      }
      if (step_num() < 31) {
         int numEdges = value().Edges.size();
         if (numEdges > 0) {
            double msg = value().PageRank / numEdges;
            for (int e : value().Edges)
               ch1.send_message(e, msg);
         } else
            ch2.add(value().PageRank);
      } else
         vote_to_halt();
   }
};

```

## Installation (for Ubuntu) ##

The installation requires the following softwares:

1. a C++ compiler (C++11 compatible)
2. cmake
3. mpich
4. JDK
5. Hadoop (only HDFS is used)

The installation can be split into two parts.
First, please make sure that all the prerequisite libraries (mpich, jdk and hdfs) are correctly installed.
Then, just by setting several environment variables, we can compile the code using cmake.

The following installation steps work on Ubuntu 16.04, and the versions of the softwares I used were:

* mpich-3.2
* OpenJDK 8
* hadoop-2.8.0

If you are using other Ubuntu release or other versions of the softwares, it is not guaranteed that the following installation steps can work as expected.
In case it happens, please refer to the installation steps of each software ([mpich](https://www.mpich.org/downloads/), [OpenJDK](), [Hadoop](http://hadoop.apache.org/releases.html)).

By the way, if you have already installed [Pregel+](http://www.cse.cuhk.edu.hk/pregelplus/) by following the instructions of Pregel+'s deployment on [this page](http://www.cse.cuhk.edu.hk/pregelplus/deploy-hadoop2.html), then you can skip the installation of these softwares, and just jump to the compilation (step 4).

### 1. Preparations ###

First, we need to install some necessary tools:

```
$ sudo apt-get update
$ sudo apt-get install cmake g++ vim wget git
```

### 2. Libraries ###

#### 2.1 MPICH

Then, download the MPI source code, compile the library and install it on your machine.
We highly recommend you to use the MPICH release, which can be download from [mpich.org](https://www.mpich.org/downloads/).
You can download the version 3.2 by the following command:

```
$ wget http://www.mpich.org/static/downloads/3.2/mpich-3.2.tar.gz
```

Next, unpack the package:

```
$ tar -xzf mpich-3.2.tar.gz
$ cd mpich-3.2
```

Compile an install the MPICH library:

```
$ ./configure --prefix=/usr/local/mpich --disable-fortran
$ make
$ sudo make install
```

Then the MPICH is installed in `/usr/local/mpich`.
To see whether the installation succeeds, type:

```
$ ls /usr/local/mpich
```

#### 2.2 OpenJDK

On ubuntu, the following command installs the OpenJDK:

```
$ sudo apt-get install openjdk-8-jdk
```

You can check whether the installation succeeds by listing the path:

```
$ ls /usr/lib/jvm/java-8-openjdk-amd64
```

#### 2.3 Hadoop

Visit this [page](http://hadoop.apache.org/releases.html), and you can find several hadoop releases in the table.
You can choose any version (we use hadoop-2.8.0 as a demonstration), and in the Tarball column, click the [binary](http://www.apache.org/dyn/closer.cgi/hadoop/common/hadoop-2.8.0/hadoop-2.8.0.tar.gz) to go to the download page.
A recommended link will be displayed on the top of the page.


Next, we use the following commands to download the package and unpack it:

```
$ wget $(url of the hadoop package)
$ tar -xzf hadoop-2.8.0.tar.gz -C /usr/local/
```

You can specify the output directory by the option `-C`.
Here, we can find the hadoop folder in `/usr/local/hadoop-2.8.0`.

### 3. Environment Variables ###

Edit the file `~/.bashrc` and add the following lines in the end:

```
export HADOOP_HOME="/usr/local/hadoop-2.8.0"
export MPICH_HOME="/usr/lib/mpich"
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"
```

If you install the MPICH, Hadoop or JDK in a place other than the paths shown above, or you are using a different version of the software, please change the path accordingly.

### 4. Compilation ###

Finally, we are going to download and compile the code, which can be done by the following commands:

```
$ git clone https://bitbucket.org/zyz915/pregel.git
$ cd pregel
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ls apps/
```

Then, you can find the executables in the `apps/` folder.

## Deployment ##

To deploy the Pregel system on a cluster, currently you can refer to the following two webpages:

* Deployment of [Pregel+](http://www.cse.cuhk.edu.hk/pregelplus/deploy-hadoop2.html) (which also works for our system)
* A setup tool for Ubuntu cluster and Amazon EC2 ([repository](https://bitbucket.org/zyz915/pregel_deployment))