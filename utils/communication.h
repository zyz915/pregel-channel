#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

#include <cassert>
#include <cstring>
#include <mpi.h>
#include <vector>

#include "global.h"

int all_sum(int v) {
	int ret;
	MPI_Allreduce(&v, &ret, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	return ret;
}

long long all_sum_ll(long long v) {
	long long ret;
	MPI_Allreduce(&v, &ret, 1, MPI_LONG_LONG, MPI_SUM, MPI_COMM_WORLD);
	return ret;
}

double all_sum_double(double v) {
	double ret;
	MPI_Allreduce(&v, &ret, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	return ret;
}

bool all_land(bool v) {
	bool ret;
	MPI_Allreduce(&v, &ret, 1, MPI_CHAR, MPI_LAND, MPI_COMM_WORLD);
	return ret;
}

bool all_lor(bool v) {
	bool ret;
	MPI_Allreduce(&v, &ret, 1, MPI_CHAR, MPI_LOR, MPI_COMM_WORLD);
	return ret;
}

double master_sum_double(double v) {
	double ret;
	MPI_Reduce(&v, &ret, 1, MPI_DOUBLE, MPI_SUM, MASTER, MPI_COMM_WORLD);
	return ret;
}

template<typename MsgT>
long long all_to_all(vector<vector<MsgT> > &sendbuf, vector<vector<MsgT> > &recvbuf) {
	int np = get_nprocs();
	int me = get_rank();
	int *sendcnt = new int[np];
	int *recvcnt = new int[np];

	for (int i = 0; i < np; i++)
		sendcnt[i] = sendbuf[i].size();
	//MPI_Alltoall(sendcnt, 1, MPI_INT, recvcnt, 1, MPI_INT, MPI_COMM_WORLD);

	MPI_Status status;
	int elem = sizeof(MsgT);
	MPI_Datatype type;
	MPI_Type_contiguous(elem, MPI_CHAR, &type);
	MPI_Type_commit(&type);

	for (int i = 1; i < np; i++) {
		int fr = (me + np - i) % np;
		int to = (me + i) % np;
		MPI_Sendrecv(
				&sendcnt[to], 1, MPI_INT, to, 0,
				&recvcnt[fr], 1, MPI_INT, fr, 0,
				MPI_COMM_WORLD, &status);
		recvbuf[fr].resize(recvcnt[fr]);
		MPI_Sendrecv(
				sendbuf[to].data(), sendcnt[to], type, to, 0,
				recvbuf[fr].data(), recvcnt[fr], type, fr, 0,
				MPI_COMM_WORLD, &status);
		sendbuf[to].clear();
	}
	recvbuf[me].swap(sendbuf[me]);

	long long totsend = 0;
	for (int i = 0; i < np; i++)
		totsend += sendcnt[i];
	//long long ret = all_sum_ll(totsend);

	long long ret = (totsend - sendcnt[me]) * elem;
	delete[] sendcnt;
	delete[] recvcnt;
	MPI_Type_free(&type);
	return ret;
}

template<typename MsgT>
void all_to_one_v(vector<MsgT> &sendbuf, vector<MsgT> &recvbuf, int rank) {
	int np = get_nprocs();
	int me = get_rank();

	int *recvcnt, *displs;
	if (me == rank) {
		recvcnt = new int[np];
		displs = new int[np + 1];
	}

	int sendcnt = sendbuf.size();
	MPI_Gather(&sendcnt, 1, MPI_INT, recvcnt, 1, MPI_INT, rank, MPI_COMM_WORLD);

	int elem = sizeof(MsgT);
	MPI_Datatype type;
	MPI_Type_contiguous(elem, MPI_CHAR, &type);
	MPI_Type_commit(&type);

	int totrecv;
	if (me == rank) {
		displs[0] = 0;
		for (int i = 0; i < np; i++)
			displs[i + 1] = displs[i] + recvcnt[i];
		totrecv = displs[np];
		recvbuf.resize(totrecv);
	}
	MPI_Gatherv(sendbuf.data(), sendcnt, type, recvbuf.data(),
			recvcnt, displs, type, rank, MPI_COMM_WORLD);

	if (me == rank)
		delete_all(recvcnt, displs);
	MPI_Type_free(&type);
}

template<typename MsgT>
long long all_to_all_v(const MsgT *sendbuf, int *sendcnt, int *sdispls, MsgT *recvbuf, int *recvcnt, int *rdispls) {
	int np = get_nprocs();
	int me = get_rank();

	MPI_Status status;
	int elem = sizeof(MsgT);
	MPI_Datatype type;
	MPI_Type_contiguous(elem, MPI_CHAR, &type);
	MPI_Type_commit(&type);

	for (int i = 1; i < np; i++) {
		int fr = (me + np - i) % np;
		int to = (me + i) % np;
		MPI_Sendrecv(
				sendbuf + sdispls[to], sendcnt[to], type, to, 0,
				recvbuf + rdispls[fr], recvcnt[fr], type, fr, 0,
				MPI_COMM_WORLD, &status);
	}
	memcpy(recvbuf + rdispls[me], sendbuf + sdispls[me], sendcnt[me] * elem);

	long long totsend = 0;
	for (int i = 0; i < np; i++)
		totsend += sendcnt[i];
	//long long ret = all_sum_ll(totsend);

	MPI_Type_free(&type);
	return (totsend - sendcnt[me]) * elem; // return in bytes
}
#endif /* COMMUNICATION_H_ */
