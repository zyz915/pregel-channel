/***
 * This file should only be used with the Domain-Specifi Language SQL-core
 * The headers and auxiliary functions are to to simplify the compilation.
 *
 * See: https://bitbucket.org/zyz915/sql-core
 */

#ifndef PREGEL_EXT_H_
#define PREGEL_EXT_H_

#include "pregel-dev.h"

#include "edge.h"
#include "direct_message.h"
#include "request_respond.h"
#include "scatter_combine.h"
#include "push_combine.h"

#include <algorithm>
#include <vector>

#endif
