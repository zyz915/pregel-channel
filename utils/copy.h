#ifndef COPY_H_
#define COPY_H_

#include <assert.h>
#include <vector>

#include "combiner.h"

using namespace std;

template<typename ValT>
class Collection {
protected:
	vector<ValT> result_;

public:
	template<typename T>
	friend void copyFrom(vector<T> &output, const Collection<T> &src);
	template<typename T>
	friend void moveFrom(vector<T> &output, Collection<T> &src);
	template<typename T>
	friend void combineFrom(vector<T> &output, const Collection<T> &src, Combiner<T> c);
};

template<typename ValT>
void copyFrom(vector<ValT> &output, const Collection<ValT> &src) {
	output = src.result_;
}

template<typename ValT>
void moveFrom(vector<ValT> &output, Collection<ValT> &src) {
	output.clear();
	output.swap(src.result_);
}

template<typename ValT>
void combineFrom(vector<ValT> &output, const Collection<ValT> &src, Combiner<ValT> c) {
	assert(output.size() == src.result_.size());
	const int locLen = src.result_.size();
	for (int i = 0; i < locLen; i++)
		c.combine(output[i], src.result_[i]);
}

#endif /* COPY_H_ */