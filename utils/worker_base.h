#ifndef WORKER_BASE_H_
#define WORKER_BASE_H_

#include "channel.h"
#include "edge.h"
#include "global.h"

template<typename KeyT>
class WorkerBase : public MessageBuffer {
protected:
	WorkerBase() {}
	virtual ~WorkerBase() {}

	/**
	 * The main computation function that will be iteratively
	 * invoked until all workers return true.
	 * @return  the local worker's decision of termination
	 *			(true = stop, false = continue)
	 */
	virtual bool compute() = 0;

	/**
	 * The finalization function will be invoked exactly once
	 * after the main computation stops.
	 */
	virtual void finalize() {}

	/* run the computation logic of Pregel */
	void run_pregel() {
		long long total_msize = 0;
		long long step_msize = 0;
		double t0 = MPI_Wtime();
		stepNumber_ = 0;
		while (true) {
			double t1 = MPI_Wtime();
			stepNumber_++;
			// compute()
			bool temp = compute();
			// whether stop or not
			bool stop = all_land(temp);
			// exchange message
			step_msize = (stop ? 0 : sync());
			total_msize += step_msize;
			double t2 = MPI_Wtime();
			/*
			if (get_rank() == MASTER) {
				printf("Superstep %d done. Time elapsed: %f seconds\n", stepNumber_, t2 - t1);
				printf("message: %lld Bytes (%s)\n", step_msize, pretty_size(step_msize));
				fflush(stdout);
			}*/
			if (stop) break;
		}
		double t3 = MPI_Wtime();
		if (get_rank() == MASTER) {
			//printf("------------------------------\n");
			printf("Total Number of Supersteps: %d\n", stepNumber_);
			printf("Total Running Time: %f second\n", t3 - t0);
			printf("Total Message Size: %lld Bytes (%s)\n",
					total_msize, pretty_size(total_msize));
			fflush(stdout);
		}
	}

public:
	/* the step number (start from 1) */
	inline int step_num() const {
		return stepNumber_;
	}

	/* The number of vertices of this process (indexes are 0 .. numv()-1) */
	virtual const int numv() const = 0;

	/* The number of vertices of a particular process */
	virtual const int numv_on_process(int rank) const = 0;

	/* The number of vertices in the whole graph */
	virtual const int total_numv() const = 0;

	/* The number of edges in the whole graph */
	virtual const long long total_nume() const = 0;

	/* Get the vertex identifier from the index */
	virtual const KeyT get_id(int idx) const = 0;

	/* Get the process id */
	virtual const int get_owner(KeyT id) const = 0;

	/* Get the index from the vertex identifier */
	virtual const int get_index(KeyT id) const = 0;

private:
	int stepNumber_ = 0;
};

#endif /* WORKER_BASE_H_ */
