#ifndef COMBINER_H_
#define COMBINER_H_

#include <climits>
#include <cstdlib>
#include <utility>

using namespace std;

template<typename ValT, typename AddT = ValT>
using CombinerT = void (*)(ValT &, const AddT &);

template<typename ValT, typename AddT = ValT>
class Combiner {
public:
	Combiner(CombinerT<ValT, AddT> c, const ValT &e):
			combine(c), identity(e) {}

public:
	CombinerT<ValT, AddT> combine;
	const ValT identity;
};

template<typename ValT, typename AddT = ValT>
Combiner<ValT, AddT> make_combiner(CombinerT<ValT, AddT> c, const ValT &e) {
	return Combiner<ValT, AddT>(c, e);
}

void c_land(bool &a, const bool &b) {
	a = a && b;
}

void c_lor(bool &a, const bool &b) {
	a = a || b;
}

template<typename T>
void c_sum(T &a, const T &b) {
	a += b;
}

template<typename T>
void c_min(T &a, const T &b) {
	if (b < a) a = b;
}

template<typename T>
void c_max(T &a, const T &b) {
	if (b > a) a = b;
}

template<typename T>
void c_rep(T &a, const T &b) {
	a = b;
}

template<typename T>
void c_random(pair<T, int> &a, const pair<T, int> &b) {
	a.second += b.second;
	if (a.second > 0 && rand() % a.second < b.second)
		a.first = b.first;
}

template<typename T>
void c_random(pair<T, double> &a, const pair<T, double> &b) {
	a.second += b.second;
	if (a.second > 0 && rand() % a.second < b.second)
		a.first = b.first;
}

#endif /* COMBINER_H_ */
