#ifndef REDUCE_COMBINE_H_
#define REDUCE_COMBINE_H_

#include <mpi.h>
#include <vector>

#include "channel.h"
#include "combiner.h"

using namespace std;

template<typename KeyT, typename MsgT>
class ReduceCombine : public Channel {
public:
	using MsgPair = pair<KeyT, MsgT>;

private:
	/* worker */
	WorkerBase<KeyT> *worker_;
	/* combiner */
	Combiner<MsgT> c_;
	/* data buffer */
	vector<vector<MsgPair> > data_buf_;
	/* return message value */
	vector<MsgT> ret_;
	/* state */
	int activated_ = 0;
	/* MPI datatype */
	MPI_Op op_;
	MPI_Datatype mpi_val_t_;

public:
	ReduceCombine(WorkerBase<KeyT> *worker, Combiner<MsgT> c, MPI_Op op):
			Channel(worker), worker_(worker), c_(c), op_(op),
			data_buf_(num_process_) {
	}

	inline const MsgT &get_message(int idx) const {
		return ret_[idx];
	}

	void collect(vector<MsgT> &msgs) {
		msgs.clear();
		ret_.swap(msgs);
	}

	void add_message(const KeyT &dest, const MsgT &msg) {
		data_buf_[worker_->get_owner(dest)].emplace_back(dest, msg);
	}

	void activate() {
		activated_ = 1;
	}

	bool active() const override {
		return (activated_ == 1);
	}

	bool empty() const {
		for (int i = 0; i < num_process_; i++)
			if (!data_buf_[i].empty())
				return false;
		return true;
	}

	long long exchange() override {
		if (activated_ != 1) return 0ll;
		int np = get_nprocs();
		int me = get_rank();
		int numv = worker_->numv();

		MPI_Request* requests = new MPI_Request[np];
		MPI_Status* statuses = new MPI_Status[np];
		vector<vector<MsgT> > reduce_buffer(np);
		ret_.resize(numv);
		fill(ret_.begin(), ret_.end(), c_.identity);

		for (int i = 0; i < np; i++) {
			reduce_buffer[i].resize(worker_->numv_on_process(i), c_.identity);
			const auto &vec = data_buf_[i];
			for (const auto &it : vec)
				c_.combine(reduce_buffer[i][worker_->get_index(it.first)], it.second);
			int numi = worker_->numv_on_process(i);
			if (me == i)
				MPI_Ireduce(reduce_buffer[i].data(), ret_.data(), numi, MPI_INT,
						op_, i, MPI_COMM_WORLD, &requests[i]);
			else
				MPI_Ireduce(reduce_buffer[i].data(), NULL, numi, MPI_INT,
						op_, i, MPI_COMM_WORLD, &requests[i]);
			data_buf_[i].clear();
		}
		MPI_Waitall(np, requests, statuses);
		delete_all(requests, statuses);
		activated_ = 0;
		return log2(np) * numv * sizeof(MsgT);
	}
};

#endif /* REDUCE_COMBINE_H_ */
