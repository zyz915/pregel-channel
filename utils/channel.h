#ifndef CHANNEL_H_
#define CHANNEL_H_

#include <vector>

#include "communication.h"
#include "global.h"

using namespace std;

class MessageBuffer;

class Channel {
public:
	/* Worker<T> is a derived class of MessageBuffer */
	Channel(MessageBuffer *mb);

	virtual bool active() const = 0;
	virtual long long exchange() = 0;

	/* several functions invoked after some events */
	virtual void after_loaded_() {}
};

class MessageBuffer {
private:
	vector<Channel*> list_;

public:
	MessageBuffer() {}
	virtual ~MessageBuffer() {}

	void add_channel(Channel *s) {
		list_.push_back(s);
	}

	void init_channels() {
		for (auto channel : list_)
			channel->after_loaded_();
	}

	long long sync() {
		int np = get_nprocs();
		long long total = 0;
		for (auto channel : list_)
			while (channel->active())
				total += channel->exchange();
		total = all_sum_ll(total); 
		return total;
	}
};

Channel::Channel(MessageBuffer *mb) {
	mb->add_channel(this);
}

#endif /* CHANNEL_H_ */
