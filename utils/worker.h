#ifndef WORKER_MATRIX_H_
#define WORKER_MATRIX_H_

#include <cstdlib>

#include "channel.h"
#include "edge.h"
#include "global.h"
#include "io.h"
#include "parser.h"

struct Param {
	// 0 = Matrix Market Exchange Format
	// 1 = Binary format (same as GeminiGraph)
	// others = unrecognizable
	int format = 0;
	// base (vertex id starts from 0 or 1)
	int base = 1;
	// read from an input file (or a directory if in_file is not specified)
	const char *in_file = "";
	const char *in_path = "";
	// make it symmetric or not
	int symm = 0;
	// number of threads
	int nthreads = 0;

	Param() = default;
};

Param parse_params(int argc, char **argv) {
	Param param;
	int p = 1;
	while (p < argc) {
		const char *op = argv[p++];
		if (op[0] != '-' || p == argc) break;
		const char *val = argv[p++];
		if (!strcmp(op, "-I"))
			param.in_file = strdup(val);
		else if (!strcmp(op, "-path"))
			param.in_path = strdup(val);
		else if (!strcmp(op, "-format") || !strcmp(op, "-F")) {
			if (!strcmp(val, "0") || !strcmp(val, "mm"))
				param.format = 0;
			else if (!strcmp(val, "1") || !strcmp(val, "bin"))
				param.format = 1;
			else {
				printf("error passing command line arguments! val = %s\n", val);
				exit(-1);
			}
		}
		else if (!strcmp(op, "-base") || !strcmp(op, "-B"))
			param.base = atoi(val);
		else if (!strcmp(op, "-symm") || !strcmp(op, "-S"))
			param.symm = atoi(val);
		else if (!strcmp(op, "-thread") || !strcmp(op, "-T"))
			param.nthreads = atoi(val);
		else {
			printf("error passing command line arguments! op = %s\n", op);
			exit(-1);
		}
	}
	if (!strlen(param.in_path) && !strlen(param.in_file)) {
		printf("usage: %s [OPTIONS]\n", argv[0]);
		printf("  where the options are:\n");
		printf("    -I <filename>  : input file\n");
		printf("    -path  <path>  : directory containing input files\n");
		printf("    -format <str>  : mm  = matrix market exchange format\n");
		printf("                     bin = Gemini's binary format\n");
		printf("    -base   <int>  : indexes are 0-base or 1-base\n");
		printf("    -symm   <int>  : convert to an undirected graph or not\n");
		printf("    -thread <int>  : number of threads each process has\n");
		exit(0);
	}
	if (param.nthreads > 0)
		set_num_threads(param.nthreads);
	return param;
}

// Support the Matrix Market Exchange format.
// See: https://math.nist.gov/MatrixMarket/formats.html
template<typename KeyT, typename EdgeT = Empty>
class Worker : public WorkerBase<KeyT> {
public:
	using EdgeBuffer = vector<EdgeTuple<KeyT, EdgeT> >;

	Worker() {}
	virtual ~Worker() {}

protected:
	struct EdgeUnit {
		int src, dst;
	};

	int load_mm_file(const Param &param, const char *file) {
		FILE *f = fopen(file, "r");
		if (f == NULL) {
			printf("error! cannot open %s", file);
			exit(-1);
		}
		printf("reading %s\n", file);
		// ignore the metadata
		char *s = fgets(buff, 1024, f);
		while (s != NULL && buff[0] == '%')
			s = fgets(buff, 1024, f);
		int rows, cols, nnz;
		if (sscanf(buff, "%d%d%d", &rows, &cols, &nnz) != 3) {
			printf("missing matrix dimension\n");
			exit(-1);
		}
		if (rows != cols) {
			printf("rows %d, cols %d, nnz %d\n", rows, cols, nnz);
			printf("invalid matrix dimension\n");
			exit(-1);
		}
		KeyT x, y;
		EdgeT w;
		while (fgets(buff, 1024, f) != NULL) {
			Parser parser(buff);
			parser.parse(x);
			parser.parse(y);
			parser.parse(w);
			x -= param.base;
			y -= param.base;
			eBuff_.push_back(EdgeTuple<KeyT, EdgeT>(x, y, w));
			if (param.symm && x != y)
				eBuff_.push_back(EdgeTuple<KeyT, EdgeT>(y, x, w));
		}
		fclose(f);
		return rows;
	}

	int load_bin_file(const Param &param, const char *file,
			EdgeUnit *es, int chunksize) {
		FILE *f = fopen(file, "rb");
		if (f == NULL) {
			printf("error! cannot open %s", file);
			exit(-1);
		}
		printf("reading %s\n", file);
		struct stat st;
		int size = 0, ptr = 0, rows = 0;
		if (stat(file, &st) == 0)
			size = st.st_size;
		int edge_unit_size = sizeof(EdgeUnit);
		EdgeT w;
		while (ptr * edge_unit_size < size) {
			int st = ptr * edge_unit_size;
			int ed = min((ptr + chunksize) * edge_unit_size, size);
			int cnt = (ed - st) / edge_unit_size;
			int count = fread(es, edge_unit_size, cnt, f);
			for (int i = 0; i < cnt; i++) {
				KeyT x = es[i].src - param.base;
				KeyT y = es[i].dst - param.base;
				if (x < 0 || y < 0) continue; // just in case
				rows = max(rows, x);
				rows = max(rows, y);
				eBuff_.push_back(EdgeTuple<KeyT, EdgeT>(x, y, w));
				if (param.symm && x != y)
					eBuff_.push_back(EdgeTuple<KeyT, EdgeT>(y, x, w));
			}
			ptr += chunksize;
		}
		fclose(f);
		return rows + 1;
	}

	// Each node stores a disjoint portion of the input graph on its local filesystem.
	// The graph is stored as text files (path/*.mtx). 
	int parallel_load_matrix_market(const Param &param) {
		int rows = 0;
		vector<string> all_files, files;
		if (strlen(param.in_file) != 0) {
			all_files.push_back(string(param.in_file));
			files = assign_files_by_rank(all_files, get_rank());
		} else {
			all_files = list_files_ext(param.in_path, ".mtx");
			files = assign_files_by_rank(all_files, get_rank());
		}		
		char fname[200];
		for (const string &file : files) {
			sprintf(fname, "%s", file.c_str());
			int r = load_mm_file(param, fname);
			rows = std::max(rows, r);
		}
		MPI_Allreduce(MPI_IN_PLACE, &rows, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
		return rows;
	}

	// Each node stores a disjoint portion of the input graph on its local filesystem.
	// The graph is stored as binary file (path/*.bin).
	int parallel_load_binary(const Param &param) {
		int rows = 0;
		int chunksize = 1 << 24;
		vector<string> all_files, files;
		EdgeUnit *es = new EdgeUnit[chunksize];
		if (strlen(param.in_file) > 0) {
			all_files.push_back(string(param.in_file));
			files = assign_files_by_rank(all_files, get_rank());
		} else {
			all_files = list_files_ext(param.in_path, ".bin");
			files = assign_files_by_rank(all_files, get_rank());
		}
		char fname[200];
		for (const string &file : files) {
			sprintf(fname, "%s", file.c_str());
			int r = load_bin_file(param, fname, es, chunksize);
			rows = std::max(rows, r);
		}
		delete[] es;
		MPI_Allreduce(MPI_IN_PLACE, &rows, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
		return rows;
	}

	void parallel_load(const Param &param) {
		double t0 = MPI_Wtime();
		int rows = 0;
		switch (param.format) {
			case 0:
				rows = parallel_load_matrix_market(param);
				break;
			case 1:
				rows = parallel_load_binary(param);
				break;
			default: {
				printf("unrecognized graph format\n");
				exit(-1);
			}
		}
		int np = get_nprocs();
		int me = get_rank();
		numEachProc_.resize(np);
		for (int i = 0; i < np; i++)
			numEachProc_[i] = (rows + np - 1 - i) / np;
		totalNumVertices_ = rows;
		numVertices_ = numEachProc_[me];
		for (int i = me; i < totalNumVertices_; i += np)
			ids_.push_back(i);

		long long m = eBuff_.size();
		totalNumEdges_ = all_sum_ll(m);
		this->load_channels(eBuff_);
		eBuff_.clear();

		double t1 = MPI_Wtime();
		if (get_rank() == MASTER) {
			printf("Load time %f seconds\n", t1 - t0);
			printf("|V| = %d, |E| = %lld\n",
					totalNumVertices_, totalNumEdges_);
			fflush(stdout);
		}
	}

public:
	/* The number of vertices of this process (indexes are 0 .. numv()-1) */
	const int numv() const override {
		return numVertices_;
	}

	/* The number of vertices of a particular process */
	const int numv_on_process(int rank) const override {
		return numEachProc_[rank];
	}

	/* The number of vertices in the whole graph */
	const int total_numv() const override {
		return totalNumVertices_;
	}

	/* The number of edges in the whole graph */
	const long long total_nume() const override {
		return totalNumEdges_;		
	}

	/* Get the vertex identifier from the index */
	const KeyT get_id(int idx) const override {
		return idx * num_process_ + my_rank_;
	}

	/* Get the process id */
	const int get_owner(KeyT id) const override {
		return id % num_process_;
	}

	/* Get the index from the vertex identifier */
	const int get_index(KeyT id) const override {
		return id / num_process_;
	}

public:
	void run(const Param &param) {
		parallel_load(param);
		MPI_Barrier(MPI_COMM_WORLD);
		this->run_pregel();
	}

	void set_in_degree(vector<int> &degree) {
		int *t = new int[total_numv()]();
		for (const auto &e : eBuff_)
			t[e.b]++;
		MPI_Allreduce(MPI_IN_PLACE, t, total_numv(), MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		degree.resize(numVertices_);
		for (int i = 0; i < numVertices_; i++)
			degree[i] = t[get_id(i)];
		delete[] t;
	}

	void set_out_degree(vector<int> &degree) {
		int *t = new int[total_numv()]();
		for (const auto &e : eBuff_)
			t[e.a]++;
		MPI_Allreduce(MPI_IN_PLACE, t, total_numv(), MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		degree.resize(numVertices_);
		for (int i = 0; i < numVertices_; i++)
			degree[i] = t[get_id(i)];
		delete[] t;
	}

	template<typename T>
	vector<int> get_range(const vector<pair<int, T> > &array) {
		vector<int> ret(numVertices_ + 1);
		ret[0] = 0;
		for (int p = 0, i = 0; i < numVertices_; i++) {
			while (p < array.size() && array[p].first == ids_[i]) p++;
			ret[i + 1] = p;
		}
		return ret;
	}

	/**
	 * Initialization of channels. This function will be invoked
	 * exactly once before the main computation is started.
	 */
	virtual void load_channels(const EdgeBuffer &es) {}

	virtual void dump(const Param &param) {}

private:
	/* The number of vertices on this process */
	int numVertices_ = 0;
	/* The number of vertices on all processs */
	int totalNumVertices_ = 0;
	/* The number of edges on all processs */
	long long totalNumEdges_ = 0;
	/* The number of vertices on each process */
	vector<int> numEachProc_;
	/* Vertex identifiers */
	vector<KeyT> ids_;
	/* Edge buffer */
	vector<EdgeTuple<KeyT, EdgeT> > eBuff_;
	/* A char buffer */
	char buff[1024];
};

#endif /* WORKER_BASE_H_ */
