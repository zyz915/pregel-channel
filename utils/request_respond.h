#ifndef REQUEST_RESPOND_H_
#define REQUEST_RESPOND_H_

#include <algorithm>
#include <numeric>
#include <vector>

#include "channel.h"
#include "global.h"
#include "worker_base.h"

using namespace std;

// KeyT should be integer! (int, unsigned, long long, etc..)
template<typename KeyT, typename ValT>
class RequestRespond : public Channel, public Collection<ValT> {
private:
	class ReqInfo_ {
	public:
		int rank, locid, index, fetch;

		ReqInfo_() = default;
		ReqInfo_(int r, int l, int i):rank(r), locid(l), index(i), fetch(0) {}

		bool operator<(const ReqInfo_ &other) const {
			return (rank != other.rank ? rank < other.rank : locid < other.locid);
		}
		bool operator!=(const ReqInfo_ &other) const {
			return (locid != other.locid || rank != other.rank);
		}
	};

private:
	/* worker */
	WorkerBase<KeyT> *worker_;
	/* all kinds of buffers */
	vector<ReqInfo_> reqs_;
	int num_reqs_;
	vector<int> sendcnt_, recvcnt_;
	vector<int> sdispls_, rdispls_;
	vector<int>  send_idx_, recv_idx_;
	vector<ValT> resp_val_, recv_val_;
	/* the return values (responses) */
	vector<ValT> &ret_ = this->result_;
	/* states */
	int request_ = 0, respond_ = 0;

public:
	RequestRespond(WorkerBase<KeyT> *worker):
			worker_(worker), Channel(worker) {}

	/* add requests (multithreaded) */
	void add_requests(const vector<KeyT> &dests) {
		int me = get_rank();
		int nt = get_num_threads();
		int np = get_nprocs();

		int *range = new int[nt + 1]();
		const int length = dests.size();
		for (int i = 0; i < nt; i++)
			range[i + 1] = range[i] + (length + i) / nt;
		int *arr2D = new int[nt * np]();
		reqs_.resize(length + 1);
		num_reqs_ = length;

		#pragma omp parallel
		{
			int id = get_thread_id();
			if (range[id + 1] > range[id]) {
				// copy array
				for (int i = range[id]; i < range[id + 1]; i++)
					reqs_[i] = ReqInfo_(worker_->get_owner(dests[i]), worker_->get_index(dests[i]), i);
				// sort & sendcnt
				std::sort(reqs_.begin() + range[id], reqs_.begin() + range[id + 1]);
				int *scnt = arr2D + id * np;
				int i = range[id];
				scnt[reqs_[i].rank] = 1;
				for (++i; i < range[id + 1]; ++i)
					if (reqs_[i - 1] != reqs_[i])
						scnt[reqs_[i].rank] += 1;
			}
		}
		// empty value
		reqs_[length].locid = -1;
		reqs_[length].rank  = -1;
		// sendcnt
		sendcnt_.resize(np);
		fill(sendcnt_.begin(), sendcnt_.end(), 0);
		int sCount = 0;
		// offset
		for (int i = 0; i < np; i++)
			for (int j = 0; j < nt; j++) {
				int t = arr2D[j * np + i];
				sendcnt_[i] += t;
				arr2D[j * np + i] = sCount;
				sCount += t;
			}
		// keys to send
		send_idx_.resize(sCount);

		#pragma omp parallel
		{
			int id = get_thread_id();
			if (range[id + 1] > range[id]) {
				// fill the unique keys
				int i = range[id];
				int *pos = arr2D + id * np;
				for (int r = 0; r < np; r++)
					if (reqs_[i].rank == r) {
						int c = pos[r];
						reqs_[i].fetch = c;
						send_idx_[c] = reqs_[i].locid;
						for (++i; reqs_[i].rank == r; ++i) {
							if (reqs_[i].locid != reqs_[i - 1].locid)
								send_idx_[++c] = reqs_[i].locid;
							reqs_[i].fetch = c;
						}
					}
			}
		}
		delete_all(arr2D, range);
		request_ = 1;
	}

	void respond(const vector<ValT> &vals) {
		assert(request_ == 2);
		int rCount = recv_idx_.size();
		resp_val_.resize(rCount);

		#pragma omp parallel for 
		for (int i = 0; i < rCount; ++i)
			resp_val_[i] = vals[recv_idx_[i]];
		recv_idx_.clear();
		respond_ = 1;
	}

	/* reset the channel to original state */
	void reset() {
		request_ = 0;
		respond_ = 0;
		sendcnt_.clear();
		recvcnt_.clear();
		sdispls_.clear();
		rdispls_.clear();
		send_idx_.clear();
		recv_idx_.clear();
		recv_val_.clear();
		resp_val_.clear();
		ret_.clear();
	}

	void collect(vector<ValT> &vals) {
		vals.clear();
		ret_.swap(vals);
	}

	/* get the response value */
	inline const ValT &get_value(int idx) {
		return ret_[idx];
	}

	/* return true if there is any request */
	bool empty() const {
		return reqs_.empty();
	}

private:
	bool active() const override {
		return request_ == 1 || respond_ == 1;
	}

	long long exchange() override {
		int np = get_nprocs();
		int me = get_rank();
		long long ret = 0;

		if (request_ == 1) {
			// sync the send/recv count (in number of elements)
			recvcnt_.resize(np);
			MPI_Alltoall(
					sendcnt_.data(), 1, MPI_INT,
					recvcnt_.data(), 1, MPI_INT,
					MPI_COMM_WORLD);
			// exchange the unique keys
			sdispls_.resize(np + 1, 0);
			rdispls_.resize(np + 1, 0);
			for (int i = 0; i < np; i++) {
				sdispls_[i + 1] = sdispls_[i] + sendcnt_[i];
				rdispls_[i + 1] = rdispls_[i] + recvcnt_[i];
			}
			int rCount = std::accumulate(recvcnt_.begin(), recvcnt_.end(), 0);
			recv_idx_.resize(rCount);

			ret += all_to_all_v(
					send_idx_.data(), sendcnt_.data(), sdispls_.data(),
					recv_idx_.data(), recvcnt_.data(), rdispls_.data());
			send_idx_.clear();
			request_ = 2;
		}

		if (respond_ == 1) {
			// exchange the values
			int rCount = std::accumulate(sendcnt_.begin(), sendcnt_.end(), 0);
			recv_val_.resize(rCount);

			ret += all_to_all_v(
					resp_val_.data(), recvcnt_.data(), rdispls_.data(),
					recv_val_.data(), sendcnt_.data(), sdispls_.data());
			resp_val_.clear();

			int length = num_reqs_;
			ret_.resize(length);

			#pragma omp parallel for
			for (int i = 0; i < length; i++)
				ret_[reqs_[i].index] = recv_val_[reqs_[i].fetch];
			recv_val_.clear();
			respond_ = 0;
		}
		return ret;
	}
};

#endif /* REQUEST_RESPOND_H_ */
