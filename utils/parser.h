#ifndef PARSER_H_
#define PARSER_H_

#include <cctype>
#include <cstdlib>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>

#include "global.h"

class Parser {
private:
	const char *str_;
	int p_;
	
public:
	Parser(const char *str) : str_(str), p_(0) {}

	char getChar() {
		while (isspace(str_[p_])) p_++;
		return str_[p_++];
	}

	int getInt() {
		while (str_[p_] != '-' && !isdigit(str_[p_])) p_++;
		int ret = 0, neg = 0;
		if (str_[p_] == '-') {
			neg = 1;
			p_ ++;
		}
		while (isdigit(str_[p_]))
			ret = (ret << 1) + (ret << 3) + (str_[p_++] - '0');
		return (neg ? -ret : ret);
	}

	unsigned getUnsigned() {
		while (!isdigit(str_[p_])) p_++;
		unsigned ret = 0;
		while (isdigit(str_[p_]))
			ret = (ret << 1) + (ret << 3) + (str_[p_++] - '0');
		return ret;
	}

	bool getBool() {
		while (!isdigit(str_[p_])) p_++;
		unsigned ret = 0;
		while (isdigit(str_[p_]))
			ret = (ret << 1) + (ret << 3) + (str_[p_++] - '0');
		return (ret != 0);
	}

	double getDouble() {
		while (str_[p_] != '-' && str_[p_] != '.' && !isdigit(str_[p_])) p_++;
		int i = 0, neg = 0;
		double ret = 0, w = 0.1;
		if (str_[p_] == '-') {
			neg = 1;
			p_ ++;
		}
		while (isdigit(str_[p_]))
			i = (i << 1) + (i << 3) + (str_[p_++] - '0');
		if (str_[p_] == '.') {
			p_++;
			while (isdigit(str_[p_])) {
				ret += w * (str_[p_++] - '0');
				w *= 0.1;
			}
		}
		ret += i;
		return (neg ? -ret : ret);
	}

	long long getLongLong() {
		while (str_[p_] != '-' && !isdigit(str_[p_])) p_++;
		int neg = 0;
		long long ret = 0;
		if (str_[p_] == '-') {
			neg = 1;
			p_ ++;
		}
		while (isdigit(str_[p_]))
			ret = (ret << 1) + (ret << 3) + (str_[p_++] - '0');
		return (neg ? -ret : ret);
	}

	string getString() {
		while (isspace(str_[p_])) p_++;
		const char *cur = str_ + p_;
		while (str_[p_] && !isspace(str_[p_])) p_++;
		return string(cur, str_ + p_);
	}

	inline void parse(char &ret) {
		ret = this->getChar();
	}

	inline void parse(int &ret) {
		ret = this->getInt();
	}

	inline void parse(bool &ret) {
		ret = this->getBool();
	}

	inline void parse(double &ret) {
		ret = this->getDouble();
	}

	inline void parse(float &ret) {
		ret = this->getDouble();
	}

	inline void parse(long long &ret) {
		ret = this->getLongLong();
	}

	inline void parse(unsigned &ret) {
		ret = this->getUnsigned();
	}

	inline void parse(size_t &ret) {
		ret = this->getInt();
	}

	inline void parse(string &ret) {
		ret = this->getString();
	}

	inline void parse(Empty &ret) {
		/* nothing */
	}

	template<typename T1, typename T2>
	void parse(std::pair<T1, T2> &ret) {
		parse(ret.first);
		parse(ret.second);
	}

	void parse(std::vector<int> &ret) {
		int l = this->getInt();
		for (int i = 0; i < l; i++)
			ret.push_back(this->getInt());
	}

	template<typename T>
	void parse(std::vector<T> &ret) {
		int l = this->getInt();
		ret.resize(l);
		for (int i = 0; i < l; i++)
			parse(ret[i]);
	}

	template<typename T>
	void parse(std::set<T> &ret) {
		int l = this->getInt();
		T x;
		for (int i = 0; i < l; i++) {
			parse(x);
			ret.insert(x);
		}
	}

	template<typename T>
	void parse(std::unordered_set<T> &ret) {
		int l = this->getInt();
		T x;
		for (int i = 0; i < l; i++) {
			parse(x);
			ret.insert(x);
		}
	}

	template<typename T1, typename T2>
	void parse(std::map<T1, T2> &ret) {
		int l = this->getInt();
		pair<T1, T2> x;
		for (int i = 0; i < l; i++) {
			parse(x);
			ret.insert(x);
		}
	}

	template<typename T1, typename T2>
	void parse(std::unordered_map<T1, T2> &ret) {
		int l = this->getInt();
		pair<T1, T2> x;
		for (int i = 0; i < l; i++) {
			parse(x);
			ret.insert(x);
		}
	}
};

#endif /* PARSER_H_ */
