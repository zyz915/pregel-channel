#ifndef GLOBAL_H_
#define GLOBAL_H_

#include <algorithm>
#include <mpi.h>
#include <string>
#include <utility>

#ifdef _OPENMP
#include <omp.h>
#include <parallel/algorithm>
#include <parallel/numeric>

#define GET_OPENMP_THREAD_ID     omp_get_thread_num()
#define GET_OPENMP_NUM_THREAD    omp_get_num_threads()
#define SET_OPENMP_NUM_THREAD(x) omp_set_num_threads(x)
#define PARALLEL_SORT            __gnu_parallel::sort
#else
#define GET_OPENMP_THREAD_ID     (0)
#define GET_OPENMP_NUM_THREAD    (1)
#define SET_OPENMP_NUM_THREAD(x) 
#define PARALLEL_SORT            std::sort
#endif

using namespace std;

#define MASTER 0

static int my_rank_;
static int num_process_;
static int num_threads_;

inline int get_rank() { return my_rank_; }
inline int get_nprocs() { return num_process_; }
inline int get_num_threads() { return num_threads_; }
inline int get_thread_id() { return GET_OPENMP_THREAD_ID; }
void set_num_threads(int x) {
	SET_OPENMP_NUM_THREAD(x);
	#pragma omp parallel
	num_threads_ = GET_OPENMP_NUM_THREAD;
}

struct Empty {};

void pregel_initialize() {
	MPI_Init(nullptr, nullptr);
	MPI_Comm_size(MPI_COMM_WORLD, &num_process_);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank_);

	#pragma omp parallel
	num_threads_ = GET_OPENMP_NUM_THREAD;
}

void pregel_finalize() {
	MPI_Finalize();
}

const char *pretty_size(long long size) {
	static char buf[64];
	if (size < 1000)
		sprintf(buf, "%lld Bytes", size);
	else if (size < 1000000)
		sprintf(buf, "%.2f KB", size * 1e-3);
	else if (size < 1000000000)
		sprintf(buf, "%.2f MB", size * 1e-6);
	else
		sprintf(buf, "%.2f GB", size * 1e-9);
	return buf;
}

int log2(int x) {
	return (x == 1 ? 0 : (sizeof(int) << 3) - __builtin_clz(x - 1));
}

template<typename A>
void delete_all(A *a) {
	delete[] a;
}

template<typename A, typename B>
void delete_all(A* a, B* b) {
	delete[] a;
	delete[] b;
}

template<typename A, typename B, typename C>
void delete_all(A* a, B* b, C *c) {
	delete[] a;
	delete_all(b, c);
}

template<typename A, typename B, typename C, typename D>
void delete_all(A* a, B* b, C *c, D *d) {
	delete[] a;
	delete_all(b, c, d);
}

#endif /* GLOBAL_H_ */
