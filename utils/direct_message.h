#ifndef DIRECT_MESSAGE_H_
#define DIRECT_MESSAGE_H_

#include <algorithm>
#include <vector>

#include "channel.h"
#include "global.h"
#include "worker_base.h"

using namespace std;

template<typename KeyT, typename MsgT>
class MsgIterator {
public:
	using MsgPair = pair<KeyT, MsgT>;

public:
	MsgIterator(const vector<MsgPair> &list, int st, int ed):
			list_(list), st_(st), ed_(ed), len_(ed - st) {}

	inline bool done() const {
		return st_ == ed_;
	}

	inline void reset() {
		st_ = ed_ - len_;
	}

	inline void next() {
		st_++;
	}

	inline const KeyT& dest() const {
		return list_[st_].first;
	}

	inline const MsgT& value() const {
		return list_[st_].second;
	}

	inline int size() const {
		return len_;
	}

private:
	/* a reference to the original vector */
	const vector<MsgPair> &list_;
	/* current position & end position */
	int st_, ed_, len_;
};

template<typename KeyT, typename MsgT>
class DirectMessage : public Channel {
public:
	using MsgPair = pair<KeyT, MsgT>;

private:
	/* worker */
	WorkerBase<KeyT> *worker_;
	/* store the messages emitted by vertices */
	vector<vector<MsgPair> > send_buf_;
	/* an internal array storing received messages */
	vector<MsgPair> array_;
	/* */
	vector<int> pos_;
	/* the status of the channel */
	int active_;

public:
	DirectMessage(WorkerBase<KeyT> *worker):
			Channel(worker), worker_(worker),
			send_buf_(num_process_),
			active_(0) {}

	/* add a message */
	inline void send_message(KeyT dst, const MsgT &msg) {
		send_buf_[worker_->get_owner(dst)].emplace_back(dst, msg);
	}

	/* collect the messages as array */
	void collect(vector<MsgPair> &ret) {
		ret.clear();
		array_.swap(ret);
		pos_.clear();
	}

	/* get the edge list for a particular vertex */
	MsgIterator<KeyT, MsgT> getIterator(int idx) const {
		return MsgIterator<KeyT, MsgT>(array_, pos_[idx], pos_[idx + 1]);
	}

	/* get the whole edge list */
	MsgIterator<KeyT, MsgT> getIterator() const {
		return MsgIterator<KeyT, MsgT>(array_, 0, array_.size());
	}

	/* activate the channel */
	void activate() {
		active_ = 1;
	}

	bool active() const override {
		return (active_);
	}

	/* implement the interface in class Channel */
	long long exchange() override {
		int np = get_nprocs();
		int me = get_rank();
		long long ret = 0;

		size_t elem = sizeof(MsgPair);
		MPI_Datatype type;
		MPI_Type_contiguous(elem, MPI_CHAR, &type);
		MPI_Type_commit(&type);

		int *sendcnt = new int[np], *sdispls = new int[np + 1]();
		int *recvcnt = new int[np], *rdispls = new int[np + 1]();

		// convert to bytes
		for (int i = 0; i < np; i++) {
			sendcnt[i] = send_buf_[i].size();
			sdispls[i + 1] = sdispls[i] + sendcnt[i];
		}
		MPI_Alltoall(sendcnt, 1, MPI_INT, recvcnt, 1, MPI_INT, MPI_COMM_WORLD);

		for (int i = 0; i < np; i++)
			rdispls[i + 1] = rdispls[i] + recvcnt[i];

		int sCount = sdispls[np];
		vector<MsgPair> sendbuf(sCount);
		#pragma omp parallel for
		for (int i = 0; i < np; ++i) {
			copy(send_buf_[i].begin(), send_buf_[i].end(),
					sendbuf.begin() + sdispls[i]);
			send_buf_[i].clear();
		}

		int rCount = rdispls[np];
		array_.resize(rCount);

		MPI_Alltoallv(
				sendbuf.data(), sendcnt, sdispls, type,
				array_.data() , recvcnt, rdispls, type,
				MPI_COMM_WORLD);
		sendbuf.clear();

		PARALLEL_SORT(array_.begin(), array_.end());

		ret += (long long)(sdispls[np] - sendcnt[me]) * elem;
		delete_all(sendcnt, recvcnt, sdispls, rdispls);
		MPI_Type_free(&type);
		active_ = 0;

		int numv = worker_->numv();
		pos_.resize(numv + 1, 0);
		int ptr = 0;
		// TODO: can be optimized by OpenMP
		for (int i = 0; i < numv; i++) {
			pos_[i] = ptr;
			while (ptr < rCount && worker_->get_id(i) == array_[ptr].first)
				ptr++;
		}
		pos_[numv] = ptr;
		return ret;
	}
};

#endif /* DIRECT_MESSAGE_H_ */
