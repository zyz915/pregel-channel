#ifndef SCATTER_COMBINE_H_
#define SCATTER_COMBINE_H_

#include <algorithm>
#include <numeric>
#include <vector>

#include "channel.h"
#include "communication.h"
#include "copy.h"
#include "edge.h"
#include "global.h"
#include "worker_base.h"

using namespace std;

// Scatter-Combine model
template<typename KeyT, typename MsgT>
class ScatterCombine : public Channel, public Collection<MsgT> {
private:
	// internal use only
	class EdgeInfo_ {
	public:
		int rank, index;
		KeyT key;

		EdgeInfo_() = default;
		EdgeInfo_(int r, KeyT k, int i):rank(r), key(k), index(i) {}

		bool operator<(const EdgeInfo_ &other) const {
			return (rank != other.rank ? rank < other.rank : key < other.key);
		}
	};

private:
	/* worker */
	WorkerBase<KeyT> *worker_;
	/* for channel setup */
	vector<EdgeInfo_> edge_links_S_; // sender side
	vector<KeyT> unique_keys_;
	int num_unique_keys_;
	vector<int> pull_indexes_, pull_index_sep_;
	vector<int> send_cnt_;
	/* store the messages emitted by vertices */
	vector<MsgT> messages_;
	vector<MsgT> &ret_ = this->result_;
	/* buffers for receiving messages */
	vector<pair<KeyT, int> > edge_links_R_; // receiver side
	vector<int> recv_idx_, recv_map_, recv_sep_;
	vector<vector<KeyT> > recv_keys_;
	vector<vector<MsgT> > recv_msgs_;
	vector<int> recv_cnt_;
	/* some optimizations */
	vector<int> s_range_, r_range_;
	/* number of threads */
	int n_threads_;
	/* the combiner and the default value */
	Combiner<MsgT> c_;
	/* internal states (op = 0: static, op = 1: dynamic) */
	int active_ = 0;

public:
	ScatterCombine(WorkerBase<KeyT> *worker, Combiner<MsgT> c):
			Channel(worker), worker_(worker), c_(c),
			recv_keys_(num_process_),
			recv_msgs_(num_process_) {}

	template<typename EdgeT>
	void load(const vector<EdgeTuple<KeyT, EdgeT> > &es) {
		// global values
		int np = get_nprocs();
		int me = get_rank();

		vector<vector<pair<KeyT, KeyT> > > sendbuf(np), recvbuf(np);
		for (int i = 0; i < es.size(); i++) {
			int proc = worker_->get_owner(es[i].a);
			sendbuf[proc].emplace_back(es[i].a, es[i].b);
		}

		// special implementation to avoid too large intermediate array
		long long comm = all_to_all(sendbuf, recvbuf);

		vector<int> sep(np + 1, 0);
		for (int i = 0; i < np; i++)
			sep[i + 1] = sep[i] + recvbuf[i].size();

		vector<pair<KeyT, KeyT> > array(sep[np]);
		#pragma omp parallel for
		for (int i = 0; i < np; i++) {
			copy(recvbuf[i].begin(), recvbuf[i].end(), array.begin() + sep[i]);
			recvbuf[i].clear();
		}
		edge_links_S_.resize(array.size());
		PARALLEL_SORT(array.begin(), array.end());

		#pragma omp parallel
		{
			int nt = get_num_threads();
			int id = get_thread_id();

			// range
			int *len = new int[nt];
			int *pos = new int[nt + 1];
			pos[0] = 0;
			for (int i = 0; i < nt; i++) {
				len[i] = (array.size() + i) / nt;
				pos[i + 1] = pos[i] + len[i];
			}

			// starting position
			KeyT key = array[pos[id]].first;
			int index = worker_->get_index(key);

			for (int i = pos[id]; i < pos[id + 1]; i++) {
				while (worker_->get_id(index) != array[i].first) index++;
				edge_links_S_[i] = EdgeInfo_(worker_->get_owner(array[i].second),
						array[i].second, index);
			}
			delete[] len;
			delete[] pos;
		}
		setup();
	}

	/* setup the communication channel */
	void setup()
	{
		int np = get_nprocs();
		int numv = worker_->numv();

		send_cnt_.resize(np);
		unique_keys_.clear();
		pull_index_sep_.clear();
		pull_indexes_.resize(edge_links_S_.size());
		//push_indexes_.resize(edge_links_S_.size());
		//vector<int> count(numv, 0);

		// in case no edge in the graph!
		if (!edge_links_S_.empty())
		{
			PARALLEL_SORT(edge_links_S_.begin(), edge_links_S_.end());

			int index0 = edge_links_S_[0].index;
			pull_indexes_[0] = index0;
			unique_keys_.push_back(edge_links_S_[0].key);
			pull_index_sep_.push_back(0);
			send_cnt_[edge_links_S_[0].rank] += 1;

			// sequential part ..
			for (int i = 1; i < edge_links_S_.size(); i++) {
				if (edge_links_S_[i].key != edge_links_S_[i - 1].key) {
					unique_keys_.push_back(edge_links_S_[i].key);
					pull_index_sep_.push_back(i);
					send_cnt_[edge_links_S_[i].rank] += 1;
				}
				const int loc_index = edge_links_S_[i].index;
				pull_indexes_[i] = loc_index;
			}
			pull_index_sep_.push_back(edge_links_S_.size());
			edge_links_S_.clear();
		}

		n_threads_ = get_num_threads();
		num_unique_keys_ = unique_keys_.size();
		s_range_.resize(n_threads_ + 1);
		s_range_[0] = 0;
		long long total = pull_index_sep_[num_unique_keys_];
		for (int i = 1; i <= n_threads_; i++) {
			int pile_sum = total * i / n_threads_;
			s_range_[i] = lower_bound(pull_index_sep_.begin(),
						pull_index_sep_.end(), pile_sum) - pull_index_sep_.begin();
		}

		recv_cnt_.resize(np);
		MPI_Alltoall(
				send_cnt_.data(), 1, MPI_INT, 
				recv_cnt_.data(), 1, MPI_INT,
				MPI_COMM_WORLD);

		int elem = sizeof(KeyT);
		MPI_Datatype type;
		MPI_Type_contiguous(elem, MPI_CHAR, &type);
		MPI_Type_commit(&type);

		int *sendcnt = new int[np], *sdispls = new int[np + 1]();
		int *recvcnt = new int[np], *rdispls = new int[np + 1]();
		
		// convert to bytes
		for (int i = 0; i < np; i++) {
			sendcnt[i] = send_cnt_[i];
			sdispls[i + 1] = sdispls[i] + sendcnt[i];
			recvcnt[i] = recv_cnt_[i];
			rdispls[i + 1] = rdispls[i] + recvcnt[i];
		}
		//ret += (long long)(sdispls[np] - sendcnt[me]) * elem;

		int rCount = std::accumulate(recv_cnt_.begin(), recv_cnt_.end(), 0);
		vector<KeyT> recv_keys(rCount);

		char *sendbuf = reinterpret_cast<char*>(&unique_keys_[0]);
		char *recvbuf = reinterpret_cast<char*>(&recv_keys[0]);

		MPI_Alltoallv(
				sendbuf, sendcnt, sdispls, type,
				recvbuf, recvcnt, rdispls, type,
				MPI_COMM_WORLD);
		unique_keys_.clear();

		edge_links_R_.resize(rCount);
		for (int i = 0; i < rCount; i++)
			edge_links_R_[i] = make_pair(recv_keys[i], i);
		recv_keys.clear();

		PARALLEL_SORT(edge_links_R_.begin(), edge_links_R_.end());

		recv_sep_.resize(numv + 1);
		recv_idx_.resize(edge_links_R_.size());
		recv_map_.resize(edge_links_R_.size());
		
		int ptr = 0;
		for (int i = 0; i < numv; i++) {
			KeyT key = worker_->get_id(i);
			recv_sep_[i] = ptr;
			while (ptr < edge_links_R_.size() && edge_links_R_[ptr].first == key) {
				recv_idx_[ptr] = edge_links_R_[ptr].second;
				recv_map_[edge_links_R_[ptr].second] = i;
				ptr++;
			}
		}
		recv_sep_[numv] = ptr;
		edge_links_R_.clear();

		total = recv_sep_[numv];
		r_range_.resize(n_threads_ + 1);
		r_range_[0] = 0;
		for (int i = 1; i <= n_threads_; i++) {
			int pile_sum = total * i / n_threads_;
			r_range_[i] = lower_bound(recv_sep_.begin(),
						recv_sep_.end(), pile_sum) - recv_sep_.begin();
		}

		delete_all(sendcnt, recvcnt, sdispls, rdispls);
	}

	/* static scatter pattern */
	void scatter(const vector<MsgT> &vals) {
		int psize = pull_indexes_.size();
		int np = get_nprocs();
		
		messages_.resize(num_unique_keys_, c_.identity);
		#pragma omp parallel for
		for (int tid = 0; tid < n_threads_; tid++)
			for (int i = s_range_[tid]; i < s_range_[tid + 1]; i++)
				for (int j = pull_index_sep_[i]; j < pull_index_sep_[i + 1]; j++)
					c_.combine(messages_[i], vals[pull_indexes_[j]]);

		active_ = 1;
	}

	/* get the combined message */
	inline const MsgT &get_message(int idx) const {
		return ret_[idx];
	}

	/* reset itself to the initial state */
	void reset() {
		edge_links_S_.clear();
		edge_links_R_.clear();
		pull_indexes_.clear();
		unique_keys_.clear();
		messages_.clear();
		ret_.clear();
		recv_idx_.clear();
		recv_map_.clear();
		s_range_.clear();
		r_range_.clear();

	}

	bool active() const override {
		return (active_);
	}

	long long exchange() override {
		int np = get_nprocs();
		int me = get_rank();
		long long ret = 0;
		
		int *sendcnt = new int[np], *sdispls = new int[np + 1]();
		int *recvcnt = new int[np], *rdispls = new int[np + 1]();

		// convert to bytes
		for (int i = 0; i < np; i++) {
			sendcnt[i] = send_cnt_[i];
			sdispls[i + 1] = sdispls[i] + sendcnt[i];
			recvcnt[i] = recv_cnt_[i];
			rdispls[i + 1] = rdispls[i] + recvcnt[i];
		}
		//ret += (long long)(sdispls[np] - sendcnt[me]) * elem;

		int rCount = std::accumulate(recv_cnt_.begin(), recv_cnt_.end(), 0);
		vector<MsgT> rbuff(rCount);

		MsgT *sendbuf = messages_.data();
		MsgT *recvbuf = rbuff.data();

		ret += all_to_all_v(
				sendbuf, sendcnt, sdispls,
				recvbuf, recvcnt, rdispls);
		messages_.clear();			

		int numv = worker_->numv();
		ret_.resize(numv);
		fill(ret_.begin(), ret_.end(), c_.identity);

		#pragma omp parallel for
		for (int tid = 0; tid < n_threads_; tid++)
			for (int i = r_range_[tid]; i < r_range_[tid + 1]; i++)
				for (int j = recv_sep_[i]; j < recv_sep_[i + 1]; j++)
					c_.combine(ret_[i], rbuff[recv_idx_[j]]);

		delete_all(sendcnt, recvcnt, sdispls, rdispls);
		active_ = 0;
		return ret;
	}
};

#endif /* SCATTER_COMBINE_H_ */
