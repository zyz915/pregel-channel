#ifndef EDGE_H_
#define EDGE_H_

#include "worker_base.h"

template<typename KeyT, typename EdgeT>
class EdgeTuple {
public:
	KeyT a, b;
	EdgeT v;

	EdgeTuple() {}
	EdgeTuple(const KeyT &a, const KeyT &b, const EdgeT &v):a(a),b(b),v(v) {}

	bool operator<(const EdgeTuple<KeyT, EdgeT> &it) const {
		return (a != it.a ? a < it.a : b < it.b);
	}
};

template<typename KeyT, typename EdgeT>
class EdgeIterator {
public:
	EdgeIterator(const vector<EdgeTuple<KeyT, EdgeT> > &list, int st, int ed):
			list_(list), st_(st), ed_(ed), len_(ed - st) {}

	inline bool done() const {
		return st_ == ed_;
	}

	inline void reset() {
		st_ = ed_ - len_;
	}

	inline void next() {
		st_++;
	}

	inline const KeyT& source() const {
		return list_[st_].a;
	}

	inline const KeyT& target() const {
		return list_[st_].b;
	}

	inline const EdgeT& value() const {
		return list_[st_].v;
	}

	inline int size() const {
		return len_;
	}

private:
	/* a reference to the original vector */
	const vector<EdgeTuple<KeyT, EdgeT> > &list_;
	/* current position & end position */
	int st_, ed_, len_;
};

template<typename KeyT, typename EdgeT>
class Edge : public Channel {
public:
	using TupleT = EdgeTuple<KeyT, EdgeT>;

private:
	/* worker */
	WorkerBase<KeyT> *worker_;
	/* edges sent to other workers */
	vector<vector<TupleT> > data_buf_;
	/* an internal array storing received edges */
	vector<TupleT> array_;
	/* the return messages for each vertex */
	vector<int> pos_;
	/* the status of the channel */
	int status_, elem_;
	/* MPI datatype */
	MPI_Datatype mpi_tuple_t_;

public:
	Edge(WorkerBase<KeyT> *worker):
			Channel(worker), worker_(worker),
			data_buf_(num_process_),
			status_(0) {
		elem_ = sizeof(EdgeT);
		MPI_Type_contiguous(elem_, MPI_CHAR, &mpi_tuple_t_);
		MPI_Type_commit(&mpi_tuple_t_);
	}

	Edge(const Edge<KeyT, EdgeT> &other):Channel(other.worker_) {
		worker_ = other.worker_;
		data_buf_.resize(get_nprocs());
		array_ = other.array_;
		pos_ = other.pos_;
		status_ = 0;
	}

	void load(const vector<EdgeTuple<KeyT, EdgeT> > &es, bool invert = false) {
		// global values
		int np = get_nprocs();
		int me = get_rank();

		vector<vector<TupleT> > sendbuf(np), recvbuf(np);
		if (!invert)
			for (int i = 0; i < es.size(); i++) {
				int proc = worker_->get_owner(es[i].a);
				sendbuf[proc].push_back(es[i]);
			}
		else
			for (int i = 0; i < es.size(); i++) {
				int proc = worker_->get_owner(es[i].b);
				sendbuf[proc].push_back(TupleT(es[i].b, es[i].a, es[i].v));
			}
		// special implementation to avoid too large intermediate array
		long long comm = all_to_all(sendbuf, recvbuf);

		vector<int> sep(np + 1, 0);
		for (int i = 0; i < np; i++)
			sep[i + 1] = sep[i] + recvbuf[i].size();

		array_.resize(sep[np]);
		#pragma omp parallel for
		for (int i = 0; i < np; i++) {
			copy(recvbuf[i].begin(), recvbuf[i].end(), array_.begin() + sep[i]);
			recvbuf[i].clear();
		}

		PARALLEL_SORT(array_.begin(), array_.end());

		int numv = worker_->numv();
		pos_.resize(numv + 1, 0);
		int ptr = 0, rCount = array_.size();
		// TODO: can be optimized by OpenMP
		for (int i = 0; i < numv; i++) {
			pos_[i] = ptr;
			while (ptr < rCount && worker_->get_id(i) == array_[ptr].a) ptr++;
		}
		pos_[numv] = ptr;
	}

	void collect(vector<pair<KeyT, KeyT> > &Edge) {
		Edge.resize(array_.size());
		#pragma omp parallel for
		for (int i = 0; i < array_.size(); i++)
			Edge[i] = make_pair(array_[i].a, array_[i].b);
		array_.clear();
		pos_.clear();
	}

	void collect(vector<pair<KeyT, pair<KeyT, EdgeT> > > &Edge) {
		Edge.resize(array_.size());
		#pragma omp parallel for
		for (int i = 0; i < array_.size(); i++)
			Edge[i] = make_pair(array_[i].a,
					make_pair(array_[i].b, array_[i].v));
		array_.clear();
		pos_.clear();
	}

	/* add an edge (thread unsafe!) */
	inline void add_edge(const KeyT &a, const KeyT &b, const EdgeT &w) {
		data_buf_[worker_->get_owner(a)].push_back(TupleT(a, b, w));
	}

	inline void add_edge(TupleT t) {
		data_buf_[worker_->get_owner(t.a)].push_back(t);
	}

	void activate() {
		status_ = 1;
	}

	/* get the edge list for a particular vertex */
	EdgeIterator<KeyT, EdgeT> getIterator(int idx) const {
		return EdgeIterator<KeyT, EdgeT>(array_, pos_[idx], pos_[idx + 1]);
	}

	/* get the whole edge list */
	EdgeIterator<KeyT, EdgeT> getIterator() const {
		return EdgeIterator<KeyT, EdgeT>(array_, 0, array_.size());
	}

	/* modify the edge value */
	void set_value(int index, const EdgeT &val) {
		array_[index].v = val;
	}

	/* read the edge value */
	const EdgeT& get_value(int index) const {
		return array_[index].v;
	}

	/* get the edge tuple */
	const EdgeTuple<KeyT, EdgeT>& get_edge(int index) const {
		return array_[index];
	}

	/* read the edge value */
	EdgeT& get_value(int index) {
		array_[index].v;
	}

	/* read the edge degree */
	const int get_degree(int index) const {
		return pos_[index + 1] - pos_[index];
	}

	/* release the memory */
	void clear() {
		for (int idx = 0; idx < num_process_; idx++)
			data_buf_[idx].clear();
		array_.clear();
		pos_.clear();
		status_ = 0;
	}

	/* invert (sequential) */
	void invert() {
		for (int idx = 0; idx < num_process_; idx++)
			data_buf_[idx].clear();

		for (int i = 0; i < array_.size(); i++)
			data_buf_[worker_->get_owner(array_[i].b)].push_back(
					TupleT(array_[i].b, array_[i].a, array_[i].v));

		array_.clear();
		status_ = 1;
	}

private:
	bool active() const override {
		return status_ == 1;
	}

	long long exchange() override {
		if (status_ != 1)
			return 0ll;
		
		int np = get_nprocs();
		int me = get_rank();

		int *sendcnt = new int[np], *sdispls = new int[np + 1];
		int *recvcnt = new int[np], *rdispls = new int[np + 1];

		for (int i = 0; i < np; i++)
			sendcnt[i] = data_buf_[i].size();

		MPI_Alltoall(sendcnt, 1, MPI_INT, recvcnt, 1, MPI_INT, MPI_COMM_WORLD);
		
		sdispls[0] = 0;
		rdispls[0] = 0;
		for (int i = 0; i < np; i++) {
			sdispls[i + 1] = sdispls[i] + sendcnt[i];
			rdispls[i + 1] = rdispls[i] + recvcnt[i];
		}

		long long ret = (long long)(sdispls[np] - sendcnt[me]) * elem_;
		TupleT *sendbuf = new TupleT[sdispls[np]];

		#pragma omp parallel for
		for (int i = 0; i < np; i++) {
			std::copy(data_buf_[i].begin(), data_buf_[i].end(), sendbuf + sdispls[i]);
			data_buf_[i].clear(); // save space!
		}

		int rCount = rdispls[np];
		array_.resize(rCount);
		char *recvbuf = reinterpret_cast<char*>(&array_[0]); // reuse the array's space

		MPI_Alltoallv((const char*)
				sendbuf, sendcnt, sdispls, mpi_tuple_t_, 
				recvbuf, recvcnt, rdispls, mpi_tuple_t_,
				MPI_COMM_WORLD);

		delete[] sendbuf;
		delete[] sendcnt;
		delete[] recvcnt;
		delete[] sdispls;
		delete[] rdispls;

		PARALLEL_SORT(array_.begin(), array_.end());

		int numv = worker_->numv();
		pos_.resize(numv + 1, 0);
		int ptr = 0;
		// TODO: can be optimized by OpenMP
		for (int i = 0; i < numv; i++) {
			pos_[i] = ptr;
			while (ptr < rCount && worker_->get_id(i) == array_[ptr].a) ptr++;
		}
		pos_[numv] = ptr;

		status_ = 0;
		return ret; 
	}

public:
	const vector<EdgeTuple<KeyT, EdgeT> > &get_edges() const {
		return array_;
	}

	vector<EdgeTuple<KeyT, EdgeT> > &get_edges() {
		return array_;
	}
};

#endif
