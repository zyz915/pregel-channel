#ifndef IO_H_
#define IO_H_

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <dirent.h>
#include <sys/stat.h>
#include <vector>
#include <mpi.h>

using namespace std;

bool file_exists(const char *name) {
  struct stat buffer;   
  return (stat (name, &buffer) == 0); 
}

vector<string> list_files_ext(const char *path, const char *extension)
{
	vector<string> result;
	DIR *dir;
	struct dirent *ent;
	char fname[256];
	if ((dir = opendir(path)) == NULL) {
		printf("cannot load files in %s\n", path);
		exit(-1);
	}
	string ext(extension);
	while ((ent = readdir(dir)) != NULL) {
		string entry(ent->d_name);
		size_t pos = entry.rfind(ext);
		if (pos != string::npos && pos == entry.length() - ext.length()) {
			sprintf(fname, "%s/%s", path, entry.c_str());
			result.push_back(fname);
		}
	}
	if (closedir(dir) != 0) {
		printf("cannot close dir %s\n", path);
		exit(-1);
	}
	return result;
}

vector<string> assign_files_by_rank(const vector<string> &files, int rank)
{
	char fname[256];
	sprintf(fname, "%d.proc", rank);
	FILE *f = fopen(fname, "w");
	MPI_Barrier(MPI_COMM_WORLD);

	vector<string> procs = list_files_ext(".", "proc");
	int nfiles = files.size();
	int nprocs = procs.size();
	int index = nprocs;
	MPI_Barrier(MPI_COMM_WORLD);
	remove(fname);

	sprintf(fname, "./%d.proc", rank);
	for (int i = 0; i < nprocs; i++)
		if (!strcmp(fname, procs[i].c_str()))
			index = i;

	vector<string> ret;
	if (index == nprocs) {
		printf("something wrong\n");
		exit(1);
	} else {
		for (int i = index; i < nfiles; i += nprocs)
			ret.push_back(files[i]);
	}
	return ret;
}

#endif /* IO_H_ */
