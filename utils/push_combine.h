#ifndef PUSH_COMBINE_H_
#define PUSH_COMBINE_H_

#include <vector>

#include "channel.h"
#include "combiner.h"

using namespace std;

template<typename KeyT, typename ValT>
void serialize(const vector<pair<KeyT, ValT> > &vec, char *buff, int size)
{
	int K = sizeof(KeyT), V = sizeof(ValT);
	if (K + V == sizeof(pair<KeyT, ValT>))
		memcpy(buff, vec.data(), size * (K + V));
	else {
		for (const auto &it : vec) {
			memcpy(buff, &(it.first), K);
			memcpy(buff + K, &(it.second), V);
			buff = buff + K + V;
		}
	}
}

template<typename KeyT, typename ValT>
void deserialize(vector<pair<KeyT, ValT> > &vec, const char *buff, int size)
{
	vec.resize(size);
	int K = sizeof(KeyT), V = sizeof(ValT);
	if (K + V == sizeof(pair<KeyT, ValT>))
		memcpy(vec.data(), buff, size * (K + V));
	else {
		for (auto &it : vec) {
			memcpy(&(it.first), buff, K);
			memcpy(&(it.second), buff + K, V);
			buff = buff + K + V;
		}
	}
}

template<typename KeyT, typename MsgT>
class PushCombine : public Channel, public Collection<MsgT> {
public:
	using MsgPair = pair<KeyT, MsgT>;

private:
	/* worker */
	WorkerBase<KeyT> *worker_;
	/* combiner */
	Combiner<MsgT> c_;
	/* data buffer */
	vector<vector<MsgPair> > data_buf_;
	/* return message value */
	vector<MsgT> &ret_ = this->result_;
	/* state */
	int activated_ = 0;

public:
	PushCombine(WorkerBase<KeyT> *worker, Combiner<MsgT> c):
			Channel(worker), worker_(worker), c_(c),
			data_buf_(num_process_) {}

	inline const MsgT &get_message(int idx) const {
		return ret_[idx];
	}

	void collect(vector<MsgT> &msgs) {
		msgs.clear();
		ret_.swap(msgs);
	}

	void add_message(const KeyT &dest, const MsgT &msg) {
		data_buf_[worker_->get_owner(dest)].emplace_back(dest, msg);
	}

	void activate() {
		activated_ = 1;
	}

	bool active() const override {
		return (activated_ == 1);
	}

	bool empty() const {
		for (int i = 0; i < num_process_; i++)
			if (!data_buf_[i].empty())
				return false;
		return true;
	}

	Combiner<MsgT> getCombiner() const {
		return c_;
	}

	long long exchange() override {
		if (activated_ != 1) return 0ll;
		int np = get_nprocs();
		int me = get_rank();

		#pragma omp parallel for
		for (int tid = 0; tid < np; tid++)
			if (!data_buf_[tid].empty()) {
				auto &vec = data_buf_[tid];
				sort(vec.begin(), vec.end());
				int ptr = 0;
				for (int i = 1; i < vec.size(); i++)
					if (vec[i].first == vec[ptr].first)
						c_.combine(vec[ptr].second, vec[i].second);
					else
						vec[++ptr] = vec[i];
				data_buf_[tid].resize(ptr + 1);
			}

		size_t elem = sizeof(KeyT) + sizeof(MsgT);
		MPI_Datatype type;
		MPI_Type_contiguous(elem, MPI_CHAR, &type);
		MPI_Type_commit(&type);
		
		int *sendcnt = new int[np], *sdispls = new int[np + 1]();
		int *recvcnt = new int[np], *rdispls = new int[np + 1]();

		for (int i = 0; i < np; i++) {
			sendcnt[i] = data_buf_[i].size();
			sdispls[i + 1] = sdispls[i] + sendcnt[i];
		}

		vector<vector<MsgPair> > recv_buf_(np);
		long long ret = all_to_all(data_buf_, recv_buf_);
		recv_buf_.swap(data_buf_);
		
		int rCount = 0;
		for (int i = 0; i < np; i++) {
			rCount += data_buf_[i].size();
			rdispls[i + 1] = rdispls[i] + data_buf_[i].size();
		}
		vector<MsgPair> recv_vec(rCount);

		#pragma omp parallel for
		for (int i = 0; i < np; i++) {
			copy(data_buf_[i].begin(), data_buf_[i].end(),
					recv_vec.begin() + rdispls[i]);
			data_buf_[i].clear();
		}
		PARALLEL_SORT(recv_vec.begin(), recv_vec.end());

		int index = 0;
		ret_.resize(worker_->numv());
		fill(ret_.begin(), ret_.end(), c_.identity);
		for (int i = 0; i < recv_vec.size(); i++) {
			while (worker_->get_id(index) != recv_vec[i].first) index++;
			c_.combine(ret_[index], recv_vec[i].second);
		}

		delete_all(sendcnt, recvcnt, sdispls, rdispls);
		MPI_Type_free(&type);
		activated_ = 0;
		return ret;
	}
};

#endif /* PUSH_COMBINE_H_ */
