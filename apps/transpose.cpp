#include "pregel-dev.h"

using namespace std;

class TestWorker : public Worker<int> {
private:
	Edge<int, Empty> edge;
public:
	TestWorker():edge(this) {}

	void load_channels(const EdgeBuffer &es) override {
		// Edge's load function implements the transpose
		edge.load(es, true);
	}

	bool compute() override {
		return true;
	}

	void output() {
		int lsum = edge.getIterator().size();
		printf("process %d: #edges %d\n", get_rank(), lsum);
	}

};

int main(int argc, char **argv) {
	pregel_initialize();
	TestWorker worker;
	worker.run(parse_params(argc, argv));
	worker.output();
	pregel_finalize();
	return 0;
}
