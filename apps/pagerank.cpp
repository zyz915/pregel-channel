#include "pregel-dev.h"
#include "scatter_combine.h"

using namespace std;

class PageRankWorker : public Worker<int> {
protected:
	vector<double> PageRank;
	vector<int> Degree;

	ScatterCombine<int, double> MSG;
	double g_sink;

public:
	PageRankWorker():MSG(this, make_combiner(c_sum, 0.0)) {}

	void load_channels(const EdgeBuffer &es) override {
		PageRank.resize(numv(), 1.0);
		set_out_degree(Degree);
		MSG.load(es);
	}

	bool compute() override {
		if (step_num() > 1) {
			double g = (g_sink * 0.85 + 0.15 * total_numv()) / total_numv();
			for (int u = 0; u < numv(); u++)
				PageRank[u] = 0.85 * MSG.get_message(u) + g;
			return step_num() == 31;
		}
		if (step_num() <= 30) {
			double sink = 0;
			for (int u = 0; u < numv(); u++)
				if (Degree[u] == 0)
					sink += PageRank[u];
				else
					PageRank[u] /= Degree[u];
			MSG.scatter(PageRank);
			g_sink = all_sum_double(sink);
		}
		return false;
	}

	void output(int TOP = 30) {
		// print the top 100 ranks
		vector<pair<double, int> > ranks, top_ranks;
		for (int i = 0; i < numv(); i++)
			ranks.push_back(make_pair(PageRank[i], get_id(i)));
		int lo = 0, hi = ranks.size();
		while (lo < TOP && hi > TOP) {
			double pivot = ranks[(hi + lo) >> 1].first;
			int ii = lo, jj = hi - 1;
			while (ii < jj) {
				if (ranks[ii].first > pivot)
					ii++;
				else {
					swap(ranks[ii], ranks[jj]);
					jj--;
				}
			}
			if (ii >= hi) {
				printf("error! ii %d, jj %d, hi %d, pivot %f\n", ii, jj, hi, pivot);
				for (int i = lo; i < hi; i++)
					printf("%d %f\n", i, ranks[i].first);
			}
			assert(ii < hi);
			if (ii >= TOP)
				hi = ii;
			else {
				while (ii < hi && ranks[ii].first == pivot)
					ii++;
				lo = ii;
			}
		}
		ranks.resize(hi); // top ranks
		all_to_one_v(ranks, top_ranks, 0);
		if (get_rank() == 0) {
			sort(top_ranks.begin(), top_ranks.end(), std::greater<pair<double, int> >());
			for (int i = 0; i < top_ranks.size() && i < TOP; i++)
				printf("index %8d  pagerank %f\n", top_ranks[i].second, top_ranks[i].first);
		}
	}
};

int main(int argc, char **argv) {
	pregel_initialize();
	PageRankWorker worker;
	worker.run(parse_params(argc, argv));
	worker.output();
	pregel_finalize();
	return 0;
}
