/* implement the minimum spanning forest algorithm */

#include <cstdio>
#include <vector>
#include <cstdint>

#include "pregel-dev.h"
#include "direct_message.h"
#include "request_respond.h"

struct SuperEdge {
	int u, v, w; // original info
	int sv; // supervertex

	SuperEdge() = default;
	SuperEdge(int u, int v, int w, int sv):
			u(u), v(v), w(w), sv(sv) {}

	bool operator<(const SuperEdge &it) const {
		return (w != it.w ? w < it.w : sv < it.sv);
	}
};

class MSF : public Worker<int, int> {
private:
	int phase, iters;

	vector<int> P, GP, Active;
	vector<SuperEdge> Store;
	vector<SuperEdge> CSR;
	vector<EdgeTuple<int, int> > MSF_result;
	vector<int> Pos;

	Edge<int, int> edge;
	DirectMessage<int, int> MSG_1;
	DirectMessage<int, SuperEdge> MSG_2;
	RequestRespond<int, int> REQ;

public:
	MSF():phase(1), iters(0), edge(this), MSG_1(this), REQ(this), MSG_2(this) {}

	~MSF() {}

	void load_channels(const EdgeBuffer &es) override {
		edge.load(es);
		Store.resize(numv()); // selected edge
		P.resize(numv()); // parent
		GP.resize(numv()); // grandparent
		Active.resize(numv(), 1);
		Pos.resize(numv() + 1);
	}

	bool compute() override {
		if (phase == 1) {
			int activeCnt = 0;
			Pos[0] = 0;
			for (int u = 0; u < numv(); u++) {
				Store[u] = SuperEdge(0, 0, INT_MAX, get_id(u));
				auto it = edge.getIterator(u);
				for (; !it.done(); it.next()) {
					SuperEdge se(it.source(), it.target(), it.value(), it.target());
					CSR.push_back(se); // <id, weight>
					if (se < Store[u])
						Store[u] = se;
				}
				Pos[u + 1] = CSR.size();
				P[u] = Store[u].sv;
				if (P[u] < get_id(u)) {
					activeCnt += 1;
					MSG_1.send_message(P[u], get_id(u));
				}
				if (it.size() == 0)
					Active[u] = false;
			}
			MSG_1.activate();
			edge.clear();
			phase = 2;
			return activeCnt == 0;
		}
		if (phase == 2) {
			for (int u = 0; u < numv(); u++)
				// only root vertices are active
				if (Active[u]) {
					// Check whether a vertex is a supervertex. Two cases:
					// (1) P[u] = get_id(u)
					// (2) it points to some other vertex, and that vertex
					//     also points to it (and thus has sent message to it)
					bool sv = (get_id(u) == P[u]);
					auto msg = MSG_1.getIterator(u);
					for (; !msg.done(); msg.next())
						sv = sv || (msg.value() == P[u]);
					if (sv)
						P[u] = get_id(u);
					else {
						MSF_result.push_back(EdgeTuple<int, int>(
								Store[u].u, Store[u].v, Store[u].w));
					}
				}
			// shortcutting
			REQ.add_requests(P);
			phase = 3;
			// if all the vertices are supervertices, terminate the computation
			return false;
		}
		if (phase == 3) {
			REQ.respond(P);
			phase = 4;
			return false;
		}
		if (phase == 4) {
			moveFrom(GP, REQ);
			int nonStars = 0;
			for (int u = 0; u < numv(); u++)
				if (P[u] != GP[u])
					nonStars += 1;
			nonStars = all_sum(nonStars);
			P = GP;
			if (nonStars > 0) {
				REQ.add_requests(P);
				phase = 3; // do the shortcutting again
			} else {
				vector<int> reqs(CSR.size());
				for (int i = 0; i < CSR.size(); i++)
					reqs[i] = CSR[i].v;
				REQ.add_requests(reqs);
				phase = 5;
			}
			return false;
		}
		if (phase == 5) {
			REQ.respond(P);
			phase = 6;
			return false;
		}
		if (phase == 6) {
			vector<int> parent, pos(numv() + 1);
			moveFrom(parent, REQ);
			pos[0] = 0;
			int loc = 0;
			for (int u = 0; u < numv(); u++) {
				SuperEdge minVal(0, 0, INT_MAX, 0);
				for (int i = Pos[u]; i < Pos[u + 1]; i++)
					if (P[u] != parent[i]) {
						CSR[i].sv = parent[i];
						CSR[loc++] = CSR[i];
						if (CSR[i] < minVal)
							minVal = CSR[i];
					}
				if (minVal.w != INT_MAX)
					MSG_2.send_message(P[u], minVal);
				Active[u] = (P[u] == get_id(u));
				pos[u + 1] = loc;
			}
			CSR.resize(loc);
			Pos.swap(pos);
			MSG_2.activate();
			phase = 7;
			return false;
		}
		if (phase == 7) {
			int activeCnt = 0;
			for (int u = 0; u < numv(); u++)
				if (Active[u]) {
					auto it = MSG_2.getIterator(u);
					if (it.size() == 0) {
						Active[u] = false;
						continue;
					}
					Store[u] = SuperEdge(0, 0, INT_MAX, get_id(u));
					for (; !it.done(); it.next())
						if (it.value() < Store[u])
							Store[u] = it.value();
					P[u] = Store[u].sv;
					if (P[u] < get_id(u)) {
						activeCnt += 1;
						MSG_1.send_message(P[u], get_id(u));
					}
					if (P[u] == get_id(u))
						Active[u] = false;
				}
			MSG_1.activate();
			phase = 2;
			activeCnt = all_sum(activeCnt);
			if (get_rank() == MASTER) {
				printf("Iteration %d: active count %d\n", ++iters, activeCnt);
			}
			return activeCnt == 0;
		}
		return true;
	}

	void output() {
		int tot_c = MSF_result.size();
		long long tot_w = 0;
		for (int u = 0; u < MSF_result.size(); u++)
			tot_w += MSF_result[u].v; // weight
		tot_c = all_sum(tot_c);
		tot_w = all_sum_ll(tot_w);
		if (get_rank() == 0) {
			printf("number of components: %d\n", total_numv() - tot_c);
			printf("total weight of MSF: %lld\n", tot_w);
		}
	}
};

int main(int argc, char **argv) {
	pregel_initialize();
	MSF worker;
	worker.run(parse_params(argc, argv));
	worker.output();
	pregel_finalize();
	return 0;
}
