// finding Connect Componets using the FastSV algorithm

#include <cstdio>
#include <utility>
#include <vector>
#include <limits>

#include "pregel-dev.h"
#include "push_combine.h"
#include "request_respond.h"
#include "scatter_combine.h"

using namespace std;

class FastSV : public Worker<int> {
private:
	int phase;
	vector<int> P, GP; // parent & grandparent
	vector<int> Dup;   // duplication of grandparent
	vector<int> MNGP;  // minimum neighbor's parent
	
	ScatterCombine<int, int> MSG_1;
	PushCombine<int, int> MSG_2;
	RequestRespond<int, int> REQ;

	Combiner<int> combiner;

public:
	FastSV() : phase(1), REQ(this),
			MSG_1(this, make_combiner(c_min, INT_MAX)),
			MSG_2(this, make_combiner(c_min, INT_MAX)),
			combiner(make_combiner(c_min, INT_MAX)) {}

	~FastSV() {}

	void load_channels(const typename Worker<int>::EdgeBuffer &es) override {
		const int locLen = this->numv();
		P.resize(locLen);
		GP.resize(locLen);
		MNGP.resize(locLen);
		Dup.resize(locLen);
		MSG_1.load(es);
	}
	
	bool compute() override {
		const int locLen = this->numv();
		if (phase == 1) {
			for (int u = 0; u < locLen; u++)
				P[u] = GP[u] = Dup[u] = MNGP[u] = get_id(u);
			// every vertex send their grandparent to all neighbors
			// in the next superstep, neighbors receive the min value
			MSG_1.scatter(GP);
			phase = 2;
			return false;
		}
		if (phase == 2) {
			combineFrom(MNGP, MSG_1, combiner);
			for (int u = 0; u < locLen; u++)
				if (MNGP[u] < GP[u]) {
					MSG_2.add_message(P[u], MNGP[u]); // normal message
					P[u] = MNGP[u]; // aggressive hooking
				} else
					P[u] = GP[u];  // shortcutting
			MSG_2.activate();
			phase = 3;
			return false;
		}
		if (phase == 3) {
			combineFrom(P, MSG_2, combiner);
			REQ.add_requests(P); // recalculate the grandparent
			phase = 4;
			return false;
		}
		if (phase == 4) {
			REQ.respond(P);
			phase = 5;
			return false;
		}
		if (phase == 5) {
			moveFrom(GP, REQ);
			bool stop = (GP == Dup);
			bool all_stop = all_land(stop);
			if (!all_stop) {
				MSG_1.scatter(GP);
				copy(GP.begin(), GP.begin() + locLen, Dup.begin());
			}
			phase = 2;
			return all_stop;

		}
		return true;
	}

	void output() {
		int sum = 0;
		for (int u = 0; u < numv(); u++)
			if (P[u] == get_id(u))
				sum += 1;
		int num_cc = all_sum(sum);
		if (get_rank() == 0)
			printf("number of CCs: %d\n", num_cc);
	}
};

int main(int argc, char **argv) {
	pregel_initialize();
	FastSV worker;
	worker.run(parse_params(argc, argv));
	worker.output();
	pregel_finalize();
	return 0;
}
