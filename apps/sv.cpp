// finding connect componets using simplified Shiloach-Vishkin Algorithm (SV)

#include <cstdio>
#include <utility>
#include <vector>

#include "pregel-dev.h"
#include "push_combine.h"
#include "request_respond.h"
#include "scatter_combine.h"

using namespace std;

class SVWorker : public Worker<int> {
private:
	int phase;
	vector<int> P, GP, MNP, Dup;
	
	ScatterCombine<int, int> MSG_1;
	PushCombine<int, int> MSG_2;
	RequestRespond<int, int> REQ;

	Combiner<int> combiner;

public:
	SVWorker() : phase(1), REQ(this),
			MSG_1(this, make_combiner(c_min, INT_MAX)),
			MSG_2(this, make_combiner(c_min, INT_MAX)),
			combiner(make_combiner(c_min, INT_MAX)) {}

	~SVWorker() {}

	void load_channels(const EdgeBuffer &es) override {
		P.resize(numv());
		MNP.resize(numv());
		Dup.resize(numv());
		MSG_1.load(es);
	}
	
	bool compute() override {
		if (phase == 1) {
			for (int u = 0; u < numv(); u++)
				P[u] = MNP[u] = get_id(u);
			REQ.add_requests(P);
			phase = 2;
			return false;
		}
		if (phase == 2) {
			Dup = P;
			REQ.respond(P);
			MSG_1.scatter(P);
			phase = 3;
			return false;
		}
		if (phase == 3) {
			copyFrom(GP, REQ);
			for (int u = 0; u < numv(); u++) {
				MNP[u] = min(MNP[u], MSG_1.get_message(u));
				if (GP[u] == P[u]) {
					if (MNP[u] < P[u])
						MSG_2.add_message(P[u], MNP[u]);
				} else
					P[u] = GP[u];
			}
			GP = P;
			MSG_2.activate();
			phase = 4;
			return false;
		}
		if (phase == 4) {
			combineFrom(P, MSG_2, combiner);
			REQ.add_requests(P);
			phase = 2;
			return (P == Dup);
		}
		return true;
	}

	void output() {
		int sum = 0;
		for (int u = 0; u < numv(); u++)
			if (P[u] == get_id(u))
				sum += 1;
		int num_cc = all_sum(sum);
		if (get_rank() == 0)
			printf("number of CCs: %d\n", num_cc);
	}
};

int main(int argc, char **argv) {
	pregel_initialize();
	SVWorker worker;
	worker.run(parse_params(argc, argv));
	worker.output();
	pregel_finalize();
	return 0;
}
